#!/bin/bash

inputs_number=$#

provider_endpoint="http://10.10.10.2:5000/v2.0/"
provider_username="max:max"
provider_password="ma38x981kju\$2"

if [ $inputs_number -eq 0 ]
  then
  	echo "No arguments found, default values will be used."
elif [ $inputs_number -eq 1 ]
	then
	  	echo "One argument found, it will be used as implementation plan's path."
	  	implementation_plan_path=$1
elif [ $inputs_number -eq 3 ]
	then
	  	echo "Three arguments found, they will be used as provider params (PROVIDER_ENDPOINT - PROVIDER_USERNAME - PROVIDER_PASSWORD)."
	  	provider_endpoint=$1
	  	provider_username=$2
	  	provider_password=$3
elif [ $inputs_number -eq 4 ]
	then
	  	echo "Four arguments found, they will be used as implementation plan's path and provider params (PROVIDER_ENDPOINT - PROVIDER_USERNAME - PROVIDER_PASSWORD)."
	  	implementation_plan_path=$1
	  	provider_endpoint=$2
	  	provider_username=$3
	  	provider_password=$4
else
	  	echo "Number of input params is not correct, default values will be used!"
fi


#Export environment variables
echo "Exporting environment variables..."

export PROVIDER_ENDPOINT=$provider_endpoint
export PROVIDER_USERNAME=$provider_username
export PROVIDER_PASSWORD=$provider_password

#Run broker service
echo "Running broker service..."


if [ -z "$implementation_plan_path" ]
	then
		java -jar broker-0.1-SNAPSHOT.jar
	else
		java -jar broker-0.1-SNAPSHOT.jar $implementation_plan_path
	fi