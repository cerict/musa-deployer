package eu.specs.project.enforcement.main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager; 
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.datamodel.enforcement.Component;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.ImplementationPlan.Vm;
import eu.specs.project.enforcement.broker.ChefServiceImpl;
import eu.specs.project.enforcement.broker.ComputeServiceEucalyptus;
import eu.specs.project.enforcement.broker.ComputeServiceOpenStack;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.exceptions.ImplementationException;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.broker.utility.EnvironmentHolder;
import eu.specs.project.enforcement.broker.utility.VmRecipeHolder;
import eu.specs.project.enforcement.contextlistener.ContextListener;


public class Main {

	private static String databagId = ""; 
	private static final Logger logger = LogManager.getLogger(Main.class);


	private Main(){

	}

	public static void main(String[] args) {

//		String username = System.getenv("PROVIDER_USERNAME");
//		String password = System.getenv("PROVIDER_PASSWORD");
//		String endpoint = System.getenv("PROVIDER_ENDPOINT");
//
//		logger.debug(username +" - "+ password +" - "+endpoint +" - "+ContextListener.CHEF_SERVER_DEFAULT_VALUE+
//				" --- "+ContextListener.CHEF_SERVER_ENDPOINT);
//
//		Provisioner provisioner;
//		try {
//			provisioner = new Provisioner(System.getenv("PROVIDER_USERNAME"),System.getenv("PROVIDER_PASSWORD") ,System.getenv("PROVIDER_ENDPOINT") , "");
//			ArrayList<String> recipes = new ArrayList<String>();
//			recipes.add("WebPool::haproxy");
//			ArrayList<Vm> server = new ArrayList<ImplementationPlan.Vm>();
//			Vm a = new Vm();
//			a.setPublicIp("194.102.62.205");
//			ArrayList<ImplementationPlan.Component> components = new ArrayList<ImplementationPlan.Component>();
//			ImplementationPlan.Component comp = new ImplementationPlan.Component();
//			comp.addPrivateIp("100.100.88.208");
//			components.add(comp);
//			a.setComponents(components);
//			server.add(a);
//			
//			provisioner.executeRecipes("ec2", "57409550E4B01BFB3ED58EC1", server, recipes);
//		} catch (ImplementationException e) {
//			e.printStackTrace();
//		}


		
//		getMosClusterId();
		
		implementPlan(args);
//		implementPlanAcquiringNewVms(args);
//		getNodesByGroup("");
//		deleteVm("5742AF97E4B04F718779AF00");
//		implementPlan(args);
		//		try {
		//			testOrderExecutionRecipesOnNode(parsePlan(null));
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}

		System.exit(0);
	}
	
	
	private static String getMosClusterId(){
		try {

			BufferedReader br = new BufferedReader(new FileReader("/mos/etc/mos/environment.json")); 
			String value="";
			String line;
			while (( line = br.readLine()) != null) {
				value+=(line+"\n");
			}
			br.close();
			
			EnvironmentHolder holder = new Gson().fromJson(value, EnvironmentHolder.class);
			logger.debug("clusterid: "+holder.getMos_cluster_identifier());
			return holder.getMos_cluster_identifier();
			

		} 
		catch (IOException e) {
			logger.debug("clusterid nullo.");
			logger.trace(e);
			return null;
		}
	}


	private static void implementPlan(String[] args){

		try {

			UUID idOne = UUID.randomUUID();
			logger.debug("UUID generato vale: "+idOne);
			databagId = idOne.toString();

			ImplementationPlan myplan = null;
			if(args!=null && args.length>0)
				myplan = parsePlan(args[0]);
			else
				myplan = parsePlan(null);

			myplan.setId(databagId);

			logger.debug("myplan: "+myplan.toString());

			ObjectMapper mapper = new ObjectMapper();

			String jsonInString = mapper.writeValueAsString(myplan);
			logger.debug("jsonInString: "+jsonInString.toString());

			Provisioner p  = new Provisioner(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"), System.getenv("PROVIDER_ENDPOINT"), "");
			p.deployVms(myplan);
			
//			executeBootstrapOnNode(myplan);
			
			//			try {
			//				p.deleteVms(myplan.getSlaId().toLowerCase(), "Eucalyptus");
			//			} catch (TimeoutException e) {
			//				e.printStackTrace();
			//			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} 
		catch (ImplementationException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} 
	}

	
	private static void executeBootstrapOnNode(ImplementationPlan plan){
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance(plan.getIaas().getAppliance());
		bean.setHw(plan.getIaas().getHardware());
		bean.setProvider(plan.getIaas().getProvider());
		bean.setZone(plan.getIaas().getZone());
		ComputeServiceInterface computeService;
		String cloudAddress = System.getenv("PROVIDER_ENDPOINT");
		
		ProviderCredential provCred = new ProviderCredential(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"));
		ProviderCredentialsManager.add("provider", provCred);
		
		if(plan.getIaas().getProvider().contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (plan.getIaas().getProvider().contentEquals("Eucalyptus")
				|| plan.getIaas().getProvider().contentEquals("ec2")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}else{
			computeService=null;
		}

		ContextListener cc = new ContextListener();

		java.util.Date date= new java.util.Date();
		logger.debug(new Timestamp(date.getTime())+" | VMs creating");

		String group = "sla-"+plan.getSlaId().toLowerCase();

		NodeCredential myCred = cc.brokerCredentials();

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(),
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);
		
		NodesInfo nodesInfo = new NodesInfo();
		nodesInfo.setPublicKey(cred.getPublickey());
		nodesInfo.setPrivateKey(cred.getPrivatekey());
		nodesInfo.setRootPassword(cred.getRootpassword());
		nodesInfo.addNode(new ClusterNode("i-22e4a17a","194.102.62.231","100.100.94.116"));
		
		String attribute= "{\"implementation_plan_id\":\""+plan.getId()+"\"}";
		ChefServiceImpl chefService = new ChefServiceImpl();
		try {
			chefService.bootstrapChef(group, nodesInfo, computeService, attribute);
		} catch (ComputationException e) {
			e.printStackTrace();
		}
		
	}
	
	
	private static void implementPlanAcquiringNewVms(String[] args){

		try {

			ImplementationPlan myplan = null;
			if(args!=null && args.length>0)
				myplan = parsePlan(args[0]);
			else
				myplan = parsePlan(null);

			myplan.setId(myplan.getId());

			logger.debug("myplan: "+myplan.toString());

			ObjectMapper mapper = new ObjectMapper();

			String jsonInString = mapper.writeValueAsString(myplan);
			logger.debug("jsonInString: "+jsonInString.toString());

			Provisioner p  = new Provisioner(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"), System.getenv("PROVIDER_ENDPOINT"), "");
			p.addNewVmsToExistingGroup(myplan);
			//			try {
			//				p.deleteVms(myplan.getSlaId().toLowerCase(), "Eucalyptus");
			//			} catch (TimeoutException e) {
			//				e.printStackTrace();
			//			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} catch (ImplementationException e) {
			e.printStackTrace();
			logger.debug(e.getMessage());
		} 
	}
	
	private static void testOrderExecutionRecipesOnNode(ImplementationPlan plan){
		// I've to order in which the recipes have to be executed
		// passo di esecuzione, macchina, ricette

		HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>> hashmapImplStep = 
				new HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>>();


		// for each node
		for(int i=0;i<plan.getPools().get(0).getVms().size();i++){
			//for each component of each node
			for(int j=0; j<plan.getPools().get(0).getVms().get(i).getComponents().size(); j++){

				int implementationStep = plan.getPools().get(0).getVms().
						get(i).getComponents().get(j).getImplementationStep();

				String temp = plan.getPools().get(0).getVms().get(i).getComponents().get(j).getCookbook()
						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(j).getRecipe();

				HashMap<Integer, ArrayList<VmRecipeHolder>> hashmapRecipes = new HashMap<Integer, ArrayList<VmRecipeHolder>>();

				if(!hashmapImplStep.containsKey(implementationStep)){
					ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

					VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
					vmRecipeHolder.addRecipestoList(temp);

					vmRecipesHolder.add(vmRecipeHolder);
					hashmapRecipes.put(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder);
					hashmapImplStep.put(implementationStep, hashmapRecipes);
				}

				else if(hashmapImplStep.containsKey(implementationStep)){
					ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

					VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
					vmRecipeHolder.addRecipestoList(temp);

					hashmapRecipes = hashmapImplStep.get(implementationStep);

					if(hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum())==null){
						vmRecipesHolder.add(vmRecipeHolder);
						hashmapRecipes.put(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder);
						hashmapImplStep.put(implementationStep, hashmapRecipes);
					}
					else{
						ArrayList<VmRecipeHolder> vmRecipesHolder1 = hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum());

						for(int h=0;h<vmRecipeHolder.getArrayListRecipes().size();h++){
							vmRecipesHolder1.get(0).addRecipestoList(vmRecipeHolder.getArrayListRecipes().get(h));
						}


						hashmapRecipes.replace(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder1);

					}

				}
			}
		}// fine for esterno


		Set<Integer> set = hashmapImplStep.keySet();
		Iterator<Integer> it = set.iterator();


		while(it.hasNext()){

			Integer key_impl_step = it.next();
			logger.debug(" *** IMPLEMENTATION STEP: "+key_impl_step +" ***");

			Set<Integer> set2 = hashmapImplStep.get(key_impl_step).keySet();
			Iterator<Integer> itVms = set2.iterator();
			Thread[] threadsImpl = new Thread[set2.size()];
			logger.debug(" *** threadsImpl number: "+set2.size() +" ***");
			int cont = 0;

			while(itVms.hasNext()){
				Integer key2 = itVms.next();
				logger.debug("  - VM n.: "+key2);

				for(int j=0;j<hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().size();j++){
					logger.debug("   - 1 recipe that have to be executed: "+hashmapImplStep.get(key_impl_step).get(key2).size());
					logger.debug("   - 2 recipe that have to be executed: "+hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().get(j));
				}

				logger.debug("cont: "+cont);

				cont++;
			}



		}


	}


	private static ImplementationPlan parsePlan(String planPath) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		ImplementationPlan myplan;
		try {
			if(planPath==null){
				BufferedReader br = new BufferedReader(new InputStreamReader (Provisioner.class.getClassLoader().getResourceAsStream("plan_1.txt"), "UTF8")); 
//				BufferedReader br = new BufferedReader(new InputStreamReader (Provisioner.class.getClassLoader().getResourceAsStream("plan_2.txt"), "UTF8")); 
				String value="";
				String tmp;
				while (( tmp = br.readLine()) != null) {
					value+=(tmp+"\n");
				}
				br.close();


				myplan = mapper.readValue(value, ImplementationPlan.class);
				logger.debug("myplan.getId(): "+myplan.getId());
				return myplan;
			}
			else{
				Path path = Paths.get(planPath);
				String value="";
				Charset charset = Charset.forName("UTF-8");
				try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
					String line = null;
					while ((line = reader.readLine()) != null) {
						System.out.println(line);
						value+=(line+"\n");
					}
				} catch (IOException x) {
					System.err.format("IOException: %s%n", x);
					throw x;
				}


				myplan = mapper.readValue(value.toString(), ImplementationPlan.class);
				logger.debug("myplan.getId(): "+myplan.getId());
				return myplan;
			}

		} catch (JsonParseException e) {
			logger.trace(e);
			throw e;
		} catch (JsonMappingException e) {
			logger.trace(e);
			throw e;
		}
	}


	private static void deleteVms(String slaId, String provider){
		Provisioner p;
		try {
			p = new Provisioner(System.getenv("PROVIDER_USERNAME"),System.getenv("PROVIDER_PASSWORD") ,System.getenv("PROVIDER_ENDPOINT") , "");
			p.deleteVms(slaId, provider);
		} catch (ImplementationException e) {
			logger.trace(e);
		} catch (TimeoutException e) {
			logger.trace(e);
		}
	}

	private static void getNodesByGroup(String groupname){
		Provisioner p;
		try {
			p = new Provisioner(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"), System.getenv("PROVIDER_ENDPOINT"), "");
			
			ImplementationPlan plan = parsePlan(null);
			
			List<String> recipes = new ArrayList<String>();
			recipes.add("WebPool::haproxy");
			
			p.executeRecipes("Eucalyptus", "571dec1ee4b0368d5f44b848", plan.getPools().get(0).getVms(), recipes);
		} catch (ImplementationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void deleteVm(String slaId){
		Provisioner p;
		try {
			p = new Provisioner(System.getenv("PROVIDER_USERNAME"), System.getenv("PROVIDER_PASSWORD"), System.getenv("PROVIDER_ENDPOINT"), "");
			
			ImplementationPlan plan = parsePlan(null);
			p.deleteVms(slaId, "Eucalyptus");
		} catch (ImplementationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
	
}
