package eu.specs.project.enforcement.broker.exceptions;

public class ComputationException extends Exception {

    public ComputationException() {
        super();
    }

    public ComputationException(String message) {
        super(message);
    }

    public ComputationException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
