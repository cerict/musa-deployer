/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.broker;

import static com.google.common.collect.Iterables.transform;
import static org.jclouds.scriptbuilder.domain.Statements.createOrOverwriteFile;
import static org.jclouds.scriptbuilder.domain.Statements.exec;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jclouds.Constants;
import org.jclouds.ContextBuilder;
import org.jclouds.chef.ChefApi;
import org.jclouds.chef.ChefContext;
import org.jclouds.chef.config.ChefProperties;
import org.jclouds.chef.domain.BootstrapConfig;
import org.jclouds.chef.domain.ChecksumStatus;
import org.jclouds.chef.domain.CookbookVersion;
import org.jclouds.chef.domain.DatabagItem;
import org.jclouds.chef.domain.Metadata;
import org.jclouds.chef.domain.Node;
import org.jclouds.chef.domain.Resource;
import org.jclouds.chef.domain.UploadSandbox;
import org.jclouds.domain.JsonBall;
import org.jclouds.io.Payloads;
import org.jclouds.io.payloads.FilePayload;
import org.jclouds.rest.ResourceNotFoundException;
import org.jclouds.scriptbuilder.domain.CreateOrOverwriteFile;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.domain.OsFamily;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.domain.StatementList;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.NodeIterator;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.google.common.primitives.Bytes;

import eu.specs.datamodel.broker.ChefData;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.contextlistener.ContextListener;


public class ChefServiceImpl {

	private ChefApi api;
	private ChefContext context;
	private static final Pattern newLinePattern = Pattern.compile("(\\r\\n)|(\\n)");

	private String organization;
	private String organizationPK;
	private String chefServerEndpoint;
	private String username;
	private String passwordPK;
	private static final Logger logger = LogManager.getLogger(ChefServiceImpl.class);
	private boolean threadError = false;

	public ChefServiceImpl () {


		ContextListener cc = new ContextListener();
		ChefData chefData = cc.chefDataInitialization();
		this.organization = chefData.getChefServerUsername();
		this.organizationPK = chefData.getChefOrganizationPrivatKey();
		this.chefServerEndpoint = chefData.getChefEndPoint();
		this.username = chefData.getChefUserString();
		this.passwordPK = chefData.getChefUserPK();


		Properties chefConfig = new Properties();
		chefConfig.put(ChefProperties.CHEF_VALIDATOR_NAME, organization+"-validator");
		chefConfig.put(ChefProperties.CHEF_VALIDATOR_CREDENTIAL, organizationPK );
		chefConfig.put(Constants.PROPERTY_RELAX_HOSTNAME, true);
		chefConfig.put(Constants.PROPERTY_TRUST_ALL_CERTS, true);
		this.context = ContextBuilder.newBuilder("chef")
				.endpoint(chefServerEndpoint + organization)
				//				.endpoint("https://chef-server.services.WebPoolCerict.clusters.i.cloud-apps.eu/organizations/mos")
				.credentials( username, passwordPK  )
				.overrides(chefConfig)
				.buildView(ChefContext.class);

		this.api = context.unwrapApi(ChefApi.class);
	}

	public void bootstrapChef(String group, NodesInfo nodes, ComputeServiceInterface cloudservice, String attribute) throws ComputationException{
		logger.debug("bootstrapChef");
//		logger.debug("bootstrapChef");
		JsonBall attrs=new JsonBall(attribute);

		BootstrapConfig bootConfig = BootstrapConfig.builder().attributes(attrs).build();
		logger.debug("bootstrapconfig build completed");
		context.getChefService().updateBootstrapConfigForGroup(group, bootConfig);
		logger.debug("updateBootstrapConfigForGroup completed");
		Statement bootstrap = prepareBootstrapChefScript(group);

		logger.debug("createBootstrapScriptForGroup bootstrap code: "+bootstrap.render(OsFamily.UNIX).toString());

		Thread[] threads = new Thread[nodes.getNodes().size()];
		
//		final Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
//		    public void uncaughtException(Thread th, Throwable ex) {
//		        System.out.println("Uncaught exception: " + ex);
//		        threadError=true;
//		    }
//		};
		
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i] = cloudservice.threadExecuteStatementsOnNode("root", nodes.getNodes().get(i), bootstrap, nodes.getPrivateKey(), false);
			} catch (Exception e) {
				logger.error( e);
			}
			
			
//			threads[i].setUncaughtExceptionHandler(handler);
			threads[i].start();
		}

		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException ignore) {
				logger.error(ignore);
			}
		}
		
		if(threadError){
			throw new ComputationException("Bootstrap execution failed");
		}
		logger.debug("after check thread error");

	}

	private void  executeRecipesOnNode(ClusterNode node, List<String> runList, String group, ComputeServiceInterface compute, NodeCredential nodecred) {

		Node oldnode = api.getNode(group+"-"+node.getPrivateIP());		 
		Node updated = Node.builder()
				.name(group+"-"+node.getPrivateIP())
				.automaticAttributes(oldnode.getAutomaticAttributes())
				.defaultAttributes(oldnode.getDefaultAttributes())
				.environment(oldnode.getEnvironment())
				.normalAttributes(oldnode.getNormalAttributes())
				.overrideAttributes(oldnode.getOverrideAttributes())
				.runList(runList)
				.build();

		api.updateNode(updated);

		String[] activeClient= {"chef-client > /var/log/chefclient.log"};	
		try {
			compute.executeScriptOnNode("root", node,activeClient,nodecred.getPrivatekey(),false);
		} catch (NoSuchElementException e) {
			logger.error( e);
		} catch (IllegalStateException e) {
			logger.error( e);
		} catch (Exception e) {
			logger.error( e);
		}

	}

	public void  executeRecipesOnNodes(List<ClusterNode> nodes , List<String> runList,String group,ComputeServiceInterface compute,NodeCredential nodecred) {

		for(ClusterNode node : nodes)
			executeRecipesOnNode(node,runList,group,compute,nodecred);
	}


	public class ExecuteRecipesOnNode  extends Thread{

		private ClusterNode node;
		private List<String> runList;
		private String group;
		private String privateKey;
		private ComputeServiceInterface compute;


		public ExecuteRecipesOnNode(ClusterNode node,
				List<String> runList, String group, String privateKey,
				ComputeServiceInterface compute) {
			super();

			this.node = node;
			this.runList = runList;
			this.group = group;
			this.privateKey = privateKey;
			this.compute = compute;
		}

		@Override
		public void run(){ 

			Node oldnode = api.getNode(group+"-"+node.getPrivateIP()); 

			if(oldnode==null)
				logger.debug("oldnode è nullo :-(");
			logger.debug("group - node.getPrivateIP() -> "+group+"-"+node.getPrivateIP());

			logger.debug("Recipes to execute: {}", runList);

			if(oldnode!=null && oldnode.getRunList()!=null && oldnode.getRunList().size() > 0){
				logger.debug("Old run list: {}", oldnode.getRunList());
				int pos = 0;
				for (String recipe : oldnode.getRunList()) {
					if (!runList.contains(recipe)) {
						runList.add(pos++, recipe);
					}
				}
				logger.debug("New run list: {}", runList);
			}

			long startTime = System.currentTimeMillis();
			logger.debug("Executing recipes {} on the node {}:{}...", runList, group, node.getPrivateIP());

			Node updated = Node.builder()
					.name(group+"-"+node.getPrivateIP())
					.automaticAttributes(oldnode.getAutomaticAttributes())
					.defaultAttributes(oldnode.getDefaultAttributes())
					.environment(oldnode.getEnvironment())
					.normalAttributes(oldnode.getNormalAttributes())
					.overrideAttributes(oldnode.getOverrideAttributes())
					.runList(runList)
					.build();

			api.updateNode(updated);

			String[] activeClient= {"chef-client > /var/log/chefclient.log"};	
			try {
				compute.executeScriptOnNode("root", node, activeClient, privateKey, false);
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				logger.error( e);
			} catch (IllegalStateException e) {
				e.printStackTrace();
				logger.error( e);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error( e);
			}
			logger.debug("Recipes {} executed successfully on the node {}:{} in {} seconds.",
					runList, group, node.getPrivateIP(), (System.currentTimeMillis() - startTime) / 1000.0);
		}
	}

	public Thread threadExecuteRecipesOnNode(ClusterNode node,	List<String> runList, String group, String privateKey, ComputeServiceInterface compute){
		return new ExecuteRecipesOnNode(node, runList, group, privateKey, compute);
	}


	public void uploadDatabagItem(String databagName,String databagItemId,String databagItemValue) throws ResourceNotFoundException{
		DatabagItem item =new DatabagItem(databagItemId,databagItemValue);
		try{
			api.createDatabagItem(databagName, item);
		}
		catch(ResourceNotFoundException e){
			throw e;
		}

	}

	public DatabagItem getDatabagItem(String databagName, String databagItemId){
		return api.getDatabagItem(databagName, databagItemId);
	}

	public DatabagItem updateDatabagItem(String databagName, String databagItemId, String value){
		DatabagItem item =new DatabagItem(databagItemId,value);
		return api.updateDatabagItem(databagName, item);
	}

	//creation of the bootstrap statement
	public Statement prepareBootstrapChefScript(String groupName) {
		BootstrapConfig config = null;
		try {
			config = context.getChefService().getBootstrapConfigForGroup(groupName);
		} catch (Exception ex) {
			logger.error(ex);
		}

		String chefConfigDir = "{root}etc{fs}chef";
		String chefBootFile = chefConfigDir + "{fs}first-boot.json";

		ImmutableList.Builder<Statement> statements = ImmutableList.builder();

		statements.add(new LiteralStatement("curl -q -s -S -L --connect-timeout 10 --max-time 600 --retry 20 -X GET  https://www.opscode.com/chef/install.sh |(bash)"));
		statements.add(exec("{md} " + chefConfigDir));
		if (config.getSslCAFile() != null) {
			statements.add(createOrOverwriteFile(chefConfigDir + "{fs}chef-server.crt",
					Splitter.on(newLinePattern).split(config.getSslCAFile())));
		}
		statements.add(createClientRbFile(chefConfigDir + "/client.rb", groupName, "", this.chefServerEndpoint+this.organization,this.organization+"-validator"));
		statements.add(CreateOrOverwriteFile.builder().path(chefConfigDir + "/validation.pem")
				.lines(Splitter.on(newLinePattern).split(this.organizationPK)).build());
		statements.add(createAttributesFile(chefBootFile, config));
		statements.add(exec("chef-client -j " + chefBootFile));

		return new StatementList(statements.build());
	}

	private Statement createClientRbFile(String clientRbFile, String group, String nodeName, String endpoint, String validatorName) {
		ImmutableList.Builder<String> clientRb = ImmutableList.builder();
		clientRb.add("require 'rubygems'");
		clientRb.add("require 'ohai'");
		clientRb.add("o = Ohai::System.new");
		clientRb.add("o.all_plugins");
		clientRb.add("node_name \"" + ((nodeName != null && !("").equals(nodeName)) ? nodeName + "\"" : group + "-\" + o[:ipaddress]"));
		clientRb.add("log_level :info");
		clientRb.add("log_location STDOUT");
		clientRb.add("validation_client_name \""+validatorName+"\"");
		clientRb.add("chef_server_url \""+endpoint+"\"");
		clientRb.add("ssl_verify_mode :verify_none ");
		clientRb.add("verify_api_cert false");

		return CreateOrOverwriteFile.builder().path(clientRbFile).lines(clientRb.build()).build();
	}

	private Statement createAttributesFile(String chefBootFile, BootstrapConfig config) {

		String runlist = Joiner.on(',').join(transform(config.getRunList(), new Function<String, String>() {
			@Override
			public String apply(String input) {
				return "\"" + input + "\"";
			}
		}));

		StringBuilder sb = new StringBuilder();
		sb.append("{");

		if (config.getAttributes() != null) {
			String attributes = config.getAttributes().toString();
			// Omit the opening and closing json characters
			sb.append(attributes.trim().substring(1, attributes.length() - 1));
			sb.append(",");
		}

		sb.append("\"run_list\":[").append(runlist).append("]");
		sb.append("}");

		return CreateOrOverwriteFile.builder().path(chefBootFile).lines(Collections.singleton(sb.toString())).build();
	}


	public void deleteNodes(String groupName) throws TimeoutException{
		Set<String> nodeList = api.listNodes();
		Iterator iterator = nodeList.iterator();
		for(int i=0;i<nodeList.size();i++){
			Node node = api.getNode(iterator.next().toString());
			if(node.getName().startsWith(groupName)){
				deleteNode(node.getName());
			}

		}
	}

	public void deleteNode(String nodeName) throws TimeoutException{
		Node node = api.deleteNode(nodeName);
		if(node==null)
			logger.debug("the node doesn't exsist.");
		else
			logger.debug("node with name: "+nodeName+" has been deleted");
	}


	/**
	 * Example of creation of a new cookbook programmatically
	 */

	private String prefix ="example2";
	private String version = "0.0.0";

	public void testCreateNewCookbook() throws IOException {
		// Define the file you want in the cookbook
		File file = new File(System.getProperty("user.dir"), "pom.xml");
		FilePayload content = uploadAndGetFilePayload(file);

		// Create the metadata of the cookbook
		Metadata metadata = Metadata.builder() //
				.name(prefix) //
				.version(version) //
				.description("Jclouds test uploaded cookbook") //
				.maintainer("jclouds") //
				.maintainerEmail("giancarlo@cerict.it") //
				.license("Apache 2.0") //
				.build();

		Resource resource = Resource.builder().name("ricetta").fromPayload(content).build();

		// Create a new cookbook
		CookbookVersion cookbook = CookbookVersion.builder(prefix, version) //
				.metadata(metadata) //
				.rootFile(Resource.builder().fromPayload(content).build()) //
				.recipe(resource)
				.attribute(Resource.builder().fromPayload(content).name("default.rb").build())
				.build();

		// upload the cookbook to the remote server
		api.updateCookbook(prefix, version, cookbook);
	}

	private FilePayload uploadAndGetFilePayload(File file) throws IOException {
		FilePayload content = Payloads.newFilePayload(file);
		content.getContentMetadata().setContentType("application/x-binary");

		// Get an md5 so that you can see if the server already has it or not
		content.getContentMetadata().setContentMD5(Files.asByteSource(file).hash(Hashing.md5()).asBytes());

		// Note that java collections cannot effectively do equals or hashcodes on
		// byte arrays, so let's convert to a list of bytes.
		List<Byte> md5 = Bytes.asList(content.getContentMetadata().getContentMD5());

		// Request an upload site for this file
		UploadSandbox site = api.createUploadSandboxForChecksums(ImmutableSet.of(md5));

		try {
			// Upload the file contents, if still not uploaded
			ChecksumStatus status = site.getChecksums().get(md5);
			if (status.needsUpload()) {
				api.uploadContent(status.getUrl(), content);
			}
		} catch (RuntimeException e) {
			api.commitSandbox(site.getSandboxId(), false);
			logger.error("RuntimeException", e);
		}
		return content;
	}




}
