package eu.specs.project.enforcement.broker.provisioning;
import java.awt.color.CMMException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.ImplementationPlan.Component;
import eu.specs.datamodel.enforcement.ImplementationPlan.Vm;
import eu.specs.project.enforcement.broker.ChefServiceImpl;
import eu.specs.project.enforcement.broker.ComputeServiceAmazon;
import eu.specs.project.enforcement.broker.ComputeServiceEucalyptus;
import eu.specs.project.enforcement.broker.ComputeServiceOpenStack;
import eu.specs.project.enforcement.broker.exceptions.ComputationException;
import eu.specs.project.enforcement.broker.exceptions.ImplementationException;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.broker.utility.VmRecipeHolder;
import eu.specs.project.enforcement.contextlistener.ContextListener;

public class Provisioner {
	private static String IMPL_PLAN_DATA_BAG = "implementation_plans";
	private String cloudAddress;
	private ChefServiceImpl chefService;
	private ComputeServiceInterface computeService = null;
	private ProviderCredential provCred;
	private static final Logger logger = LogManager.getLogger(Provisioner.class);
//	boolean threadError = false;


	private ObjectMapper mapper = new ObjectMapper();

	public Provisioner(String username, String password, String cloudAddress, String groupName) throws ImplementationException{

		this.cloudAddress=cloudAddress;

		provCred = new ProviderCredential(username, password);
		ProviderCredentialsManager.add("provider", provCred);

		chefService = new ChefServiceImpl();
	}


	public ImplementationPlan deployVms(ImplementationPlan plan) throws ImplementationException{
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance(plan.getIaas().getAppliance());
		bean.setHw(plan.getIaas().getHardware());
		bean.setProvider(plan.getIaas().getProvider());
		bean.setZone(plan.getIaas().getZone());

		if(plan.getIaas().getProvider().contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (plan.getIaas().getProvider().contentEquals("Eucalyptus")
				|| plan.getIaas().getProvider().contentEquals("ec2")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}else{
			computeService=null;
		}

		ContextListener cc = new ContextListener();

		java.util.Date date= new java.util.Date();
		logger.debug(new Timestamp(date.getTime())+" | VMs creating");

		String group = "sla-"+plan.getSlaId().toLowerCase();

		NodeCredential myCred = cc.brokerCredentials();

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(),
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		int vmsNumber = plan.getPools().get(0).getVms().size();

		try {
			long startTime = System.currentTimeMillis();
			ArrayList<Integer> portlist = new ArrayList<>();

			//here I compute all the ports to be opened on the security group
			List<Vm> vms = plan.getPools().get(0).getVms();
			int portList[]=null;
			for(int i=0;i<vms.size();i++){
				List<Component> components = vms.get(i).getComponents();
				for(int j=0;j<components.size();j++){
					for(int ii=0;ii<components.get(j).getFirewall().getIncoming().getPortList().size();ii++){
						if(!portlist.contains(Integer.valueOf(components.get(j).getFirewall().getIncoming().getPortList().get(ii)))){
							portlist.add(Integer.valueOf(components.get(j).getFirewall().getIncoming().getPortList().get(ii)));
						}
					}
				}
			}
			portList = new int[portlist.size()];
			for(int i=0;i<portlist.size();i++){
				logger.debug("port to be opened: "+portlist.get(i));
				portList[i]=portlist.get(i);
			}

			NodesInfo nodesInfo = computeService.createNodesInGroup(group, vmsNumber , bean,
					NodeCredentialsManager.getCredentials("def"), portList);

			if(nodesInfo.getNodes().size()<vmsNumber){
				logger.debug("there was a problem during the acquisition of the nodes, but try to remediate.");
				logger.debug("number the acquired nodes: "+nodesInfo.getNodes().size()
						+", while the Vms number that had to be acquired is: "+vmsNumber);
				int remainingVms =  vmsNumber - nodesInfo.getNodes().size();
				NodesInfo nodesInfoRemaining = computeService.createNodesInGroup(group, remainingVms , bean,
						NodeCredentialsManager.getCredentials("def"), portList);
				if(nodesInfoRemaining.getNodes().size()!=remainingVms){
					throw new ComputationException("Error during acquisition of remaing VMS");
				}
				else{
					for(int i=0;i<nodesInfoRemaining.getNodes().size();i++){
						nodesInfo.addNode(nodesInfoRemaining.getNodes().get(i));
					}
					
				}
			}

			java.util.Date myDate= new java.util.Date();
			logger.debug(new Timestamp(myDate.getTime())+" | VMs created");

			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				logger.debug("ip privato acquisito: "+s);
				logger.debug("ip pubblico acquisito: "+node.getPublicIP());
				logger.debug("node id: "+node.getId());

			}

			// update implementation plan
			try {
				for (int i = 0; i < nodesInfo.getNodes().size(); i++) {
					ImplementationPlan.Vm vm = plan.getPools().get(0).getVms().get(i);
					ClusterNode nodeInfo = nodesInfo.getNodes().get(i);
					vm.setPublicIp(nodeInfo.getPublicIP());
					if (nodeInfo.getPrivateIP() != null) {
						for (ImplementationPlan.Component component : vm.getComponents()) {
							component.addPrivateIp(nodeInfo.getPrivateIP());
						}
					}
				}
			} catch (Exception e) {
				throw new ImplementationException("Failed to update the implementation plan: " + e.getMessage(), e);
			}

			// upload updated data bag to the Chef server
			plan.setId(UUID.randomUUID().toString());
			chefService.uploadDatabagItem(IMPL_PLAN_DATA_BAG, plan.getId(), mapper.writeValueAsString(plan));

			//-----------Preparing VMs : executing environment script--------------------
			java.util.Date mydatenew= new java.util.Date();
			logger.debug(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = computeService.threadExecuteInstructionsOnNode("root", nodes.get(i), new String[]{"zypper -n install ruby || apt-get install ruby"}, cred.getPrivatekey(), true);

				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				threads[i].join();
			}

			java.util.Date date1= new java.util.Date();
			logger.debug(new Timestamp(date1.getTime())+" | VMs prepared");

			String attribute= "{\"implementation_plan_id\":\""+plan.getId()+"\"}";
			chefService.bootstrapChef(group, nodesInfo, computeService, attribute);
			//
			java.util.Date date5= new java.util.Date();
			logger.debug(new Timestamp(date5.getTime())+" | chef node bootstrapped");
			logger.debug("Nodes have been created and bootstrapped in {} seconds.",
					(System.currentTimeMillis() - startTime) / 1000.0);

			// NUOVO DA VERIFICARE
			HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>> hashmapImplStep = 
					new HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>>();

			// for each node
			for(int i=0;i<plan.getPools().get(0).getVms().size();i++){
				//for each component of each node
				for(int j=0; j<plan.getPools().get(0).getVms().get(i).getComponents().size(); j++){

					int implementationStep = plan.getPools().get(0).getVms().
							get(i).getComponents().get(j).getImplementationStep();

					String cookbook = plan.getPools().get(0).getVms().get(i).getComponents().get(j).getCookbook();
					String recipe = plan.getPools().get(0).getVms().get(i).getComponents().get(j).getRecipe();

					HashMap<Integer, ArrayList<VmRecipeHolder>> hashmapRecipes = new HashMap<Integer, ArrayList<VmRecipeHolder>>();

					if(!hashmapImplStep.containsKey(implementationStep)){
						ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

						VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
						vmRecipeHolder.addRecipesToList(recipe, cookbook);

						vmRecipesHolder.add(vmRecipeHolder);
						hashmapRecipes.put(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder);
						hashmapImplStep.put(implementationStep, hashmapRecipes);
					}

					else if(hashmapImplStep.containsKey(implementationStep)){
						ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

						VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
						vmRecipeHolder.addRecipesToList(recipe, cookbook);

						hashmapRecipes = hashmapImplStep.get(implementationStep);

						if(hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum())==null){
							vmRecipesHolder.add(vmRecipeHolder);
							hashmapRecipes.put(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder);
							hashmapImplStep.put(implementationStep, hashmapRecipes);
						}
						else{
							//							ArrayList<VmRecipeHolder> vmRecipesHolder1 = hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum());
							//							vmRecipesHolder1.add(vmRecipeHolder);
							//							hashmapRecipes.replace(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder1);

							ArrayList<VmRecipeHolder> vmRecipesHolder1 = hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum());

							for(int h=0;h<vmRecipeHolder.getArrayListRecipes().size();h++){
								vmRecipesHolder1.get(0).addRecipestoList(vmRecipeHolder.getArrayListRecipes().get(h));
							}


							hashmapRecipes.replace(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder1);
						}
					}
				}
			}// fine for esterno

			Set<Integer> set = hashmapImplStep.keySet();
			Iterator<Integer> it = set.iterator();


			while(it.hasNext()){

				Integer key_impl_step = it.next();
				logger.debug(" *** IMPLEMENTATION STEP: "+key_impl_step +" ***");

				Set<Integer> set2 = hashmapImplStep.get(key_impl_step).keySet();
				Iterator<Integer> itVms = set2.iterator();
				Thread[] threadsImpl = new Thread[set2.size()];
				logger.debug(" *** threadsImpl number: "+set2.size() +" ***");
				int cont = 0;



				while(itVms.hasNext()){
					Integer key2 = itVms.next();
					logger.debug("  - VM n.: "+key2);

					for(int j=0;j<hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().size();j++){
						logger.debug("   - recipe that have to be executed: "+hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().get(j));
					}

					logger.debug("cont: "+cont);
					threadsImpl[cont] = chefService.threadExecuteRecipesOnNode(
							nodes.get(key2), hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes(), group, myCred.getPrivatekey(), computeService);

//					Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
//						public void uncaughtException(Thread th, Throwable ex) {
//							System.out.println("Uncaught exception: " + ex);
//							threadError=true;
//						}
//					};
//
//					threadsImpl[cont].setUncaughtExceptionHandler(handler);

					threadsImpl[cont].start();

					cont++;
				}

				for(int i=0;i<cont;i++ ){
					threadsImpl[i].join();
				}


			}

			return plan;


			//			//			 VECCHIO FUNZIONANTE
			//
			//						//Executing recipes on nodes 
			//						for(int i=0;i<nodes.size();i++){
			//			
			//							List<List<String>> recipes=new ArrayList<List<String>> ();
			//							List <String> temp = new ArrayList<String>();
			//							for(int j=0; j<plan.getPools().get(0).getVms().get(i).getComponents().size(); j++){
			//								temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(j).getCookbook()
			//										+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(j).getRecipe());
			//								recipes.add(temp);
			//							}
			//			
			//							java.util.Date date6= new java.util.Date();
			//							logger.debug(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP());
			//			
			//							threads[i] = chefService.threadExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group, myCred.getPrivatekey(), computeService);
			//							threads[i].start();
			//						}
			//			
			//						for(int i=0;i<nodes.size();i++ ){
			//							threads[i].join();
			//						}
			//			
			//			
			//						java.util.Date date7= new java.util.Date();
			//						logger.debug(new Timestamp(date7.getTime())+" | recipes completed");
			//			
			//						return plan;

		} 
		catch(ComputationException compEx){
			throw new ImplementationException("bootstrap execution failed: " + compEx.getMessage(), compEx);
		}
		catch (Exception e) {
			throw new ImplementationException("Failed to deploy VMs: " + e.getMessage(), e);
		}
	}

	/**
	 * this method allows to acquire new VMs and add them to an exsisting group.
	 * the VMs that have to be acquired and configured are taken from plan input parameter
	 * considering only those VM that have not a publicIp set.
	 * @param plan
	 * @return the ImplementationPlan instance
	 * @throws ImplementationException
	 */
	public ImplementationPlan addNewVmsToExistingGroup(ImplementationPlan plan) throws ImplementationException{
		
		if(plan==null){
			logger.debug("addNewVmsToExistingGroup - the plan is null");
			throw new ImplementationException("The plan passed to addNewVmsToExistingGroup is null");
		}
		
		logger.debug("addNewVmsToExistingGroup - the plan passed is: "+new Gson().toJson(plan).toString());
		
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance(plan.getIaas().getAppliance());
		bean.setHw(plan.getIaas().getHardware());
		bean.setProvider(plan.getIaas().getProvider());
		bean.setZone(plan.getIaas().getZone());

		if(plan.getIaas().getProvider().contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (plan.getIaas().getProvider().contentEquals("Eucalyptus")
				|| plan.getIaas().getProvider().contentEquals("ec2")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}else{
			computeService=null;
			logger.debug("addNewVmsToExistingGroup - the computeService is null");
			throw new ImplementationException("addNewVmsToExistingGroup - The provider defined in the plan is different from the once defined in the code.");
		}

		ContextListener cc = new ContextListener();

		java.util.Date date= new java.util.Date();
		logger.debug(new Timestamp(date.getTime())+" | VMs creating");

		String group = "sla-"+plan.getSlaId().toLowerCase();

		NodeCredential myCred = cc.brokerCredentials();

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(),
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		int vmsNumber = 0;


		for(int i=0;i<plan.getPools().get(0).getVms().size();i++){
			if(plan.getPools().get(0).getVms().get(i).getPublicIp()==null){
				vmsNumber++;
			}
		}
		logger.debug("addNewVmsToExistingGroup - the number of new VMs that have to be acquired is: "+vmsNumber);

		List<ImplementationPlan.Vm> haProxyServers = new ArrayList<>();
		
		try {
			long startTime = System.currentTimeMillis();
			ArrayList<Integer> portlist = new ArrayList<>();

			//here I compute all the ports to be opened on the security group
			List<Vm> vms = plan.getPools().get(0).getVms();
			int portList[]=null;
			for(int i=0;i<vms.size();i++){
				List<Component> components = vms.get(i).getComponents();
				for(int j=0;j<components.size();j++){
					
					if(components.get(j).getId().equals("wp_haproxy")){
						haProxyServers.add(vms.get(i));
						logger.debug("haProxyServers has been set");
					}
					
					
					for(int ii=0;ii<components.get(j).getFirewall().getIncoming().getPortList().size();ii++){
						if(!portlist.contains(Integer.valueOf(components.get(j).getFirewall().getIncoming().getPortList().get(ii)))){
							portlist.add(Integer.valueOf(components.get(j).getFirewall().getIncoming().getPortList().get(ii)));
						}
					}
				}
			}
			portList = new int[portlist.size()];
			for(int i=0;i<portlist.size();i++){
				logger.debug("port to be opened: "+portlist.get(i));
				portList[i]=portlist.get(i);
			}

			NodesInfo nodesInfo = computeService.createNodesInGroup(group, vmsNumber , bean,
					NodeCredentialsManager.getCredentials("def"), portList);


			java.util.Date myDate= new java.util.Date();
			logger.debug(new Timestamp(myDate.getTime())+" | VMs created");

			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				logger.debug("ip privato acquisito: "+s);
				logger.debug("ip pubblico acquisito: "+node.getPublicIP());
				logger.debug("node id: "+node.getId());

			}

			// update implementation plan with new IPs
			List<Vm> newVms = new ArrayList<ImplementationPlan.Vm>();
			try {
				int counter = 0;
				for(int i=0;i<vms.size();i++){

					if(vms.get(i).getPublicIp()==null){
						
						vms.get(i).setPublicIp(nodesInfo.getNodes().get(counter).getPublicIP());

						for (ImplementationPlan.Component component : vms.get(i).getComponents()) {
							if(component.getPrivateIps()==null){
								component.setPrivateIps(new ArrayList<String>());
							}
							component.addPrivateIp(nodesInfo.getNodes().get(counter).getPrivateIP());
						}
						newVms.add(vms.get(i));
						counter++;
					}
				}

			} catch (Exception e) {
				throw new ImplementationException("addNewVmsToExistingGroup - Failed to update the implementation plan: " + e.getMessage(), e);
			}

			// upload updated data bag to the Chef server
			if(plan.getId()==null || plan.getId().equals(""))
				plan.setId(UUID.randomUUID().toString());

			chefService.updateDatabagItem(IMPL_PLAN_DATA_BAG, plan.getId(), mapper.writeValueAsString(plan));

			//-----------Preparing VMs : executing environment script--------------------
			java.util.Date mydatenew= new java.util.Date();
			logger.debug(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = computeService.threadExecuteInstructionsOnNode("root", nodes.get(i), new String[]{"zypper -n install ruby || apt-get install ruby"}, cred.getPrivatekey(), true);

				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				threads[i].join();
			}

			java.util.Date date1= new java.util.Date();
			logger.debug(new Timestamp(date1.getTime())+" | VMs prepared");

			String attribute= "{\"implementation_plan_id\":\""+plan.getId()+"\"}";
			chefService.bootstrapChef(group, nodesInfo, computeService, attribute);
			//
			java.util.Date date5= new java.util.Date();
			logger.debug(new Timestamp(date5.getTime())+" | chef node bootstrapped");
			logger.debug("Nodes have been created and bootstrapped in {} seconds.",
					(System.currentTimeMillis() - startTime) / 1000.0);

			HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>> hashmapImplStep = 
					new HashMap<Integer, HashMap<Integer, ArrayList<VmRecipeHolder>>>();

			// for each node
			for(int i=0;i<newVms.size();i++){
				//for each component of each node
				for(int j=0; j<newVms.get(i).getComponents().size(); j++){

					ImplementationPlan.Component component = newVms.get(i).getComponents().get(j);
					if (component.getRecipe() == null) {
						continue; // skip this component
					}

					int implementationStep = newVms.
							get(i).getComponents().get(j).getImplementationStep();

					String recipe = newVms.get(i).getComponents().get(j).getRecipe();
					String cookbook = newVms.get(i).getComponents().get(j).getCookbook();

					HashMap<Integer, ArrayList<VmRecipeHolder>> hashmapRecipes = new HashMap<Integer, ArrayList<VmRecipeHolder>>();

					if(!hashmapImplStep.containsKey(implementationStep)){
						ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

						VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
						vmRecipeHolder.addRecipesToList(recipe, cookbook);

						vmRecipesHolder.add(vmRecipeHolder);
						//						hashmapRecipes.put(newVms.get(i).getVmSeqNum(), vmRecipesHolder);
						hashmapRecipes.put(i, vmRecipesHolder);
						hashmapImplStep.put(implementationStep, hashmapRecipes);
					}

					else if(hashmapImplStep.containsKey(implementationStep)){
						ArrayList<VmRecipeHolder> vmRecipesHolder = new ArrayList<VmRecipeHolder>();

						VmRecipeHolder vmRecipeHolder = new VmRecipeHolder();
						vmRecipeHolder.addRecipesToList(recipe, cookbook);

						hashmapRecipes = hashmapImplStep.get(implementationStep);

						if(hashmapRecipes.get(i)==null){
							vmRecipesHolder.add(vmRecipeHolder);
							hashmapRecipes.put(i, vmRecipesHolder);
							hashmapImplStep.put(implementationStep, hashmapRecipes);
						}
						else{
							//							ArrayList<VmRecipeHolder> vmRecipesHolder1 = hashmapRecipes.get(plan.getPools().get(0).getVms().get(i).getVmSeqNum());
							//							vmRecipesHolder1.add(vmRecipeHolder);
							//							hashmapRecipes.replace(plan.getPools().get(0).getVms().get(i).getVmSeqNum(), vmRecipesHolder1);

							ArrayList<VmRecipeHolder> vmRecipesHolder1 = hashmapRecipes.get(i);

							for(int h=0;h<vmRecipeHolder.getArrayListRecipes().size();h++){
								vmRecipesHolder1.get(0).addRecipestoList(vmRecipeHolder.getArrayListRecipes().get(h));
							}


							hashmapRecipes.replace(i, vmRecipesHolder1);
						}
					}
				}
			}// fine for esterno

			Set<Integer> set = hashmapImplStep.keySet();
			logger.debug("Implementation steps: {}", set);
			Iterator<Integer> it = set.iterator();


			while(it.hasNext()){

				Integer key_impl_step = it.next();
				logger.debug(" *** IMPLEMENTATION STEP: "+key_impl_step +" ***");

				Set<Integer> set2 = hashmapImplStep.get(key_impl_step).keySet();
				Iterator<Integer> itVms = set2.iterator();
				Thread[] threadsImpl = new Thread[set2.size()];
				logger.debug(" *** threadsImpl number: "+set2.size() +" ***");
				int cont = 0;

				while(itVms.hasNext()){
					Integer key2 = itVms.next();
					logger.debug("  - VM n.: "+key2);

					for(int j=0;j<hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().size();j++){
						logger.debug("   - recipe that have to be executed: "+hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes().get(j));
					}

					logger.debug("cont: "+cont);
					threadsImpl[cont] = chefService.threadExecuteRecipesOnNode(
							nodes.get(key2), hashmapImplStep.get(key_impl_step).get(key2).get(0).getArrayListRecipes(), group, myCred.getPrivatekey(), computeService);
					threadsImpl[cont].start();

					cont++;
				}

				for(int i=0;i<cont;i++ ){
					threadsImpl[i].join();
				}


			}
			
			if(haProxyServers!=null && haProxyServers.size()>0){
				logger.debug("trying to execute recipe haproxy on node with ip: "+haProxyServers.get(0).getPublicIp());
				ArrayList<String> recipes = new ArrayList<String>();
				recipes.add("WebPool::haproxy");
				executeRecipes(plan.getIaas().getProvider(), plan.getSlaId(), haProxyServers, recipes);
			}
			else{
				logger.debug("No haproxyserver has been found");
			}
			
			
			
			

			return plan;


			//			//			 VECCHIO FUNZIONANTE
			//
			//						//Executing recipes on nodes 
			//						for(int i=0;i<nodes.size();i++){
			//			
			//							List<List<String>> recipes=new ArrayList<List<String>> ();
			//							List <String> temp = new ArrayList<String>();
			//							for(int j=0; j<plan.getPools().get(0).getVms().get(i).getComponents().size(); j++){
			//								temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(j).getCookbook()
			//										+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(j).getRecipe());
			//								recipes.add(temp);
			//							}
			//			
			//							java.util.Date date6= new java.util.Date();
			//							logger.debug(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP());
			//			
			//							threads[i] = chefService.threadExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group, myCred.getPrivatekey(), computeService);
			//							threads[i].start();
			//						}
			//			
			//						for(int i=0;i<nodes.size();i++ ){
			//							threads[i].join();
			//						}
			//			
			//			
			//						java.util.Date date7= new java.util.Date();
			//						logger.debug(new Timestamp(date7.getTime())+" | recipes completed");
			//			
			//						return plan;

		} catch (Exception e) {
			throw new ImplementationException("Failed to deploy VMs: " + e.getMessage(), e);
		}
	}


	public void executeRecipes(String provider, String slaId, List<ImplementationPlan.Vm> vms, List<String> recipes){

		if(provider.contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (provider.contentEquals("Eucalyptus") || provider.contentEquals("ec2")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}else if ( provider.contentEquals("ec2")){
			computeService = new ComputeServiceAmazon(cloudAddress, "root", provCred);
		}

		slaId = slaId.toLowerCase();
		//TODO add check to verifiy that all nodes retrievede from the provider belong to the same group and are properly registered on Chef
		//		for(int j=0;j<vms.size();j++){
		//			vms.get(j).getComponents().get(0).getPrivateIps().get(0);
		//		}

		try {
			ContextListener cc = new ContextListener();
			NodeCredential myCred = cc.brokerCredentials();

			NodeCredential cred = new NodeCredential(
					myCred.getPublickey(),
					myCred.getPrivatekey());

			NodesInfo nodes = computeService.getNodesByGroup("sla-"+slaId);
			NodesInfo myNodes = new NodesInfo();

			for(int i=0;i<vms.size();i++){
				ClusterNode clusterNode = new ClusterNode();

				for(int k=0;k<nodes.getNodes().size();k++){
					if(nodes.getNodes().get(k).getPrivateIP()!=null && nodes.getNodes().get(k).getPublicIP()!=null){
						if(nodes.getNodes().get(k).getPrivateIP().equals(vms.get(i).getComponents().get(0).getPrivateIps().get(0))
								&& nodes.getNodes().get(k).getPublicIP().equals(vms.get(i).getPublicIp())){
							clusterNode.setPublicIP(vms.get(i).getPublicIp());
							clusterNode.setPrivateIP(vms.get(i).getComponents().get(0).getPrivateIps().get(0));
							clusterNode.setId(nodes.getNodes().get(k).getId());
							myNodes.addNode(clusterNode);
						}
					}
					//					else{
					//						logger.debug("differents");
					//					}
				}
			}

			Thread[] threads=null;
			threads = new Thread[myNodes.getNodes().size()];

			for (int i=0;i<myNodes.getNodes().size();i++){
				logger.debug("node "+i+" - privateIp: "+myNodes.getNodes().get(i).getPrivateIP());
				logger.debug("node "+i+" - publicIp: "+myNodes.getNodes().get(i).getPublicIP());

				threads[i] = 
						chefService.threadExecuteRecipesOnNode(myNodes.getNodes().get(i), recipes, "sla-"+slaId, myCred.getPrivatekey(), computeService);
				threads[i].start();
			}

			for(int i=0;i<myNodes.getNodes().size();i++ ){
				threads[i].join();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteVms(String slaId, String provider) throws TimeoutException{
		logger.debug("Delete the VMs with slaId {} and provider {}", slaId, provider);
		slaId=slaId.toLowerCase();
		
		if(provider.contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (provider.contentEquals("Eucalyptus") || provider.contentEquals("ec2")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}
//		else if ( provider.contentEquals("ec2")){
//			computeService = new ComputeServiceAmazon(cloudAddress, "root", provCred);
//		}
		else
			return;

		logger.debug("Delete the VMs in the group, the security group and the network");
		computeService.destroyClusterWithName(slaId);


	}


	public void deleteListOfVM(String provider, String slaId, List<ImplementationPlan.Vm> vms){

		if(provider.contentEquals("OpenStack")){
			computeService = new ComputeServiceOpenStack(cloudAddress, "cloud", provCred);
		} else if (provider.contentEquals("Eucalyptus")){
			computeService = new ComputeServiceEucalyptus(cloudAddress, "root", provCred);
		}else if ( provider.contentEquals("ec2")){
			computeService = new ComputeServiceAmazon(cloudAddress, "root", provCred);
		}


		//TODO add check to verifiy that all nodes retrievede from the provider belong to the same group and are properly registered on Chef
		//		for(int j=0;j<vms.size();j++){
		//			vms.get(j).getComponents().get(0).getPrivateIps().get(0);
		//		}

		try {
			slaId = slaId.toLowerCase();
			NodesInfo nodes = computeService.getNodesByGroup("sla-"+slaId);
			NodesInfo myNodes = new NodesInfo();
			if(nodes==null || nodes.getNodes()==null || nodes.getNodes().size()==0){
				logger.debug("the number of Vm that belong to the same group with slaId: "+slaId+" are zero");

			}
			else{
				logger.debug("the number of Vm that belong to the same grou with slaId: "+slaId+" are " +nodes.getNodes().size());
				for(int i=0;i<vms.size();i++){
					ClusterNode clusterNode = new ClusterNode();

					for(int k=0;k<nodes.getNodes().size();k++){
						if(nodes.getNodes().get(k).getPrivateIP()!=null && nodes.getNodes().get(k).getPublicIP()!=null){
							if(nodes.getNodes().get(k).getPrivateIP().equals(vms.get(i).getComponents().get(0).getPrivateIps().get(0))
									&& nodes.getNodes().get(k).getPublicIP().equals(vms.get(i).getPublicIp())){
								clusterNode.setPublicIP(vms.get(i).getPublicIp());
								clusterNode.setPrivateIP(vms.get(i).getComponents().get(0).getPrivateIps().get(0));
								clusterNode.setId(nodes.getNodes().get(k).getId());
								myNodes.addNode(clusterNode);
							}
						}

						//					else{
						//						logger.debug("differents");
						//					}
					}
				}

				Thread[] threads=null;
				threads = new Thread[myNodes.getNodes().size()];

				if(myNodes==null || myNodes.getNodes()==null || myNodes.getNodes().size()==0){
					logger.debug("There is no VM to delete");
				}
				else{
					logger.debug("The number of VM that has to be deleted is: "+myNodes.getNodes().size());
					for (int i=0;i<myNodes.getNodes().size();i++){
						logger.debug("node "+i+" - privateIp: "+myNodes.getNodes().get(i).getPrivateIP());
						logger.debug("node "+i+" - publicIp: "+myNodes.getNodes().get(i).getPublicIP());

						final int pos = i;
						final NodesInfo finalNodes =myNodes;
						threads[i] = new Thread(new Runnable() {

							@Override
							public void run() {
								computeService.destroySingleNode(finalNodes.getNodes().get(pos));

							}
						});
						threads[i].start();
					}

					for(int i=0;i<myNodes.getNodes().size();i++ ){
						threads[i].join();
					}
				}
				

			}
			



		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateDatabagItem(ImplementationPlan plan) throws ComputationException{
		try {
			chefService.updateDatabagItem(IMPL_PLAN_DATA_BAG, plan.getId(), mapper.writeValueAsString(plan));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new ComputationException("Error updating databag item");
		}

	}

	/*
	private static void acquireAmazonVms(){
		// inserisco i valori che descrivono il provider e le sue credenziali
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance("us-east-1/ami-ff0e0696");
		bean.setHw("t1.micro");
		bean.setProvider("aws-ec2");
		bean.setZone("us-east-1");

		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		ProviderCredentialsManager.add("provider", provCred);
		CloudService cloudservice = new CloudService(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		logger.debug(new Timestamp(date.getTime())+" | VMs creating");	


		//		InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);

		String group = "sla-1";

		ContextListener cc = new ContextListener();
		NodeCredential myCred = cc.brokerCredentials();
		logger.debug("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		logger.debug("public key: "+myCred.getPublickey());

		NodeCredential cred = new NodeCredential("specs.123456", 
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , bean,
					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			logger.debug(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				logger.debug("ip privato acquisito: "+s);
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			logger.debug(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i),prepareEnvironmentAmazonScript(), cred.getPrivatekey(),false,cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			java.util.Date date1= new java.util.Date();	
			logger.debug(new Timestamp(date1.getTime())+" | VMs prepared");


			//			info.status=Status.VMs_Prepared;


			//attributo del nodo che viene letto dalla ricetta
			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		


			java.util.Date date4= new java.util.Date();	   
			logger.debug(new Timestamp(date4.getTime())+" | chef node bootstrapping");

			ChefService chefService = new ChefService(null, null, null, null, null);
			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);

			java.util.Date date5= new java.util.Date();	
			logger.debug(new Timestamp(date5.getTime())+" | chef node bootstrapped");

			//			info.status=Status.Chef_Node_Bootstrapped;

			//			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());


			List<List<String>> recipes=new ArrayList<List<String>> ();
			List <String> temp = new ArrayList<String>();
			temp.add("specs-enforcement-webpool::nginx");
			recipes.add(temp);

			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){


				java.util.Date date6= new java.util.Date();	
				logger.debug(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				

				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(i), group,myCred.getPrivatekey(), cloudservice);
				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));

			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				logger.debug(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;
		} catch (Exception e) {
			logger.trace(e);
		}
	}


	//	private static void uploadDatabag(){
	//		String databagId = UUID.randomUUID().toString().replace("-","");
	//		try {
	//			chefService.uploadDatabagItem("implementation_test",  databagId, "{\"plan_id\":\"1\",\"value\":\"30\"}");
	//		} catch (ResourceNotFoundException e) {
	//			logger.trace(e);
	//		}
	//	}
	 */


}
