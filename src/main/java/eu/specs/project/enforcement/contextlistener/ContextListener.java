/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
 */

package eu.specs.project.enforcement.contextlistener;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import eu.specs.datamodel.broker.ChefData;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.broker.utility.EnvironmentHolder;

public class ContextListener {

	private String CHEF_SERVER_ORGANIZATION="mos";
	private final static String CHEF_SERVER_DEFAULT_START="https://chef-server.services.";
	private final static String CHEF_SERVER_DEFAULT_END="/organizations/";
	public final static String CHEF_SERVER_DEFAULT_VALUE=System.getenv("CHEF_SERVER_DEFAULT_VALUE");
	public final static String CHEF_SERVER_ENDPOINT=CHEF_SERVER_DEFAULT_START+CHEF_SERVER_DEFAULT_VALUE+CHEF_SERVER_DEFAULT_END;
	private final String CHEF_USER="mos-chef-admin";
	private final String CHEF_USER_PRIVATE_KEY_FILE="mos-chef-admin.pem";
	private final String CHEF_ORGANIZATION_PRIVATE_KEY_FILE="mos-validator.pem";

	private String PROVIDER_USER_PRIVATE_KEY_FILE="provider_user_private_keyOK_RKTest.txt";
	private String PROVIDER_USER_PUBLIC_KEY_FILE="def_public_key.txt";

	private Properties prop;
	private ChefData chefdata;
	private static final Logger logger = LogManager.getLogger(Provisioner.class);

	public NodeCredential brokerCredentials(){

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader (ContextListener.class.getClassLoader().getResourceAsStream(PROVIDER_USER_PRIVATE_KEY_FILE), "UTF8")); 
			String tempPrivateKey="";
			String line;
			while (( line = br.readLine()) != null) {
				tempPrivateKey+=(line+"\n");
			}
			br.close();

			BufferedReader br2 = new BufferedReader(new InputStreamReader (ContextListener.class.getClassLoader().getResourceAsStream(PROVIDER_USER_PUBLIC_KEY_FILE), "UTF8")); 
			String tempPublicKey="";
			String line2;
			while (( line2 = br2.readLine()) != null) {
				tempPublicKey+=(line2+"\n");
			}
			br2.close();

			NodeCredential cred = new NodeCredential("", 
					new String (tempPublicKey), 
					new String  (tempPrivateKey));
			return cred;
		} 
		catch (IOException e) {
			logger.trace(e);
			return null;
		}
	}
	
	public static String getMosClusterId(){
		try {

			BufferedReader br = new BufferedReader(new FileReader("/mos/etc/mos/environment.json")); 
			String value="";
			String line;
			while (( line = br.readLine()) != null) {
				value+=(line+"\n");
			}
			br.close();
			
			EnvironmentHolder holder = new Gson().fromJson(value, EnvironmentHolder.class);
			logger.debug("clusterid: "+holder.getMos_cluster_identifier());
			return holder.getMos_cluster_identifier();
			

		} 
		catch (IOException e) {
			logger.debug("clusterid nullo.");
			logger.trace(e);
			return null;
		}
	}

	public ProviderCredential getDefaultProviderCredentials(){

		ProviderCredential providerCredential = new ProviderCredential(System.getProperty("PROVIDER_USERNAME"), System.getProperty("PROVIDER_PASSWORD"));
		return providerCredential;
	}


	public ChefData chefDataInitialization(){

		prop = new Properties();
		chefdata = new ChefData();

		try {
			chefdata.setChefServerUsername(CHEF_SERVER_ORGANIZATION);
			chefdata.setChefUserString(CHEF_USER);
			chefdata.setChefEndPoint(CHEF_SERVER_ENDPOINT);
			
			logger.debug("chef-server-endpoint: "+CHEF_SERVER_ENDPOINT);
			
			String tempChefUserPk="";
			String line;

			BufferedReader br = new BufferedReader(new InputStreamReader (ContextListener.class.getClassLoader().getResourceAsStream(CHEF_USER_PRIVATE_KEY_FILE), "UTF8")); 

			while (( line = br.readLine()) != null) {
				tempChefUserPk+=(line+"\n");
			}

			br.close();

			chefdata.setChefUserPK(tempChefUserPk);

			String tempChefOrgPk="";
			String lineOrg;

			BufferedReader brOrg = new BufferedReader(new InputStreamReader (ContextListener.class.getClassLoader().getResourceAsStream(CHEF_ORGANIZATION_PRIVATE_KEY_FILE),"UTF8")); 
			while (( lineOrg = brOrg.readLine()) != null) {
				tempChefOrgPk+=(lineOrg+"\n");
			}

			brOrg.close();
			chefdata.setChefOrganizationPrivatKey(tempChefOrgPk);

		} 
		catch (IOException e) {
			logger.trace(e);
		} 
		return chefdata;

	}

	public static String doGet(String path) throws IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget= new HttpGet(path);

		logger.debug("Executing request " + httpget.getRequestLine());

		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			@Override
			public String handleResponse(
					final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					logger.debug(status);
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			}

		};
		String responseBody = httpclient.execute(httpget, responseHandler);
		logger.debug("----------------------------------------");
		logger.debug(responseBody);

		httpclient.close();
		return responseBody;
	}

	public static String doGetUncheckSSL(String path){
		CloseableHttpResponse response =null;

		try {
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(
					sslsf).build();

			HttpGet httpGet = new HttpGet(path);
			response = httpclient.execute(httpGet);

			logger.debug(response.getStatusLine());
			HttpEntity entity = response.getEntity();
			EntityUtils.consume(entity);
		}
		catch(Exception e){
			logger.trace(e);
		}
		finally {
			try {
				response.close();
			} catch (IOException e) {
				logger.trace(e);
			}
		}
		return response.toString();
	}


	public ChefData getChefdata() {
		return chefdata;
	}

	public void setChefdata(ChefData chefdata) {
		this.chefdata = chefdata;
	}

	public String getProperty(String name){
		return prop.getProperty(name).toString();
	}
}

