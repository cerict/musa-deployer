### What does the Deployer do? ###

The Deployer allows you to acquire resources from one or many Cloud Service Provider. 


### PREREQUISITES ###

CONSIDER THAT YOU NEED TO HAVE TWO VPNs IN ORDER TO USE THE DEPLOYER:

  1. the first vpn connection is useful to access the openstack horizion interface and allows the deployer to acquire resources;
  
  2. the second vpn connection is useful to allow the deployer to configure the VMs acquired vie the deployer. 

**Please add the following line "10.10.10.2 MUSAControl" to hosts file of the machine that will run the deployer component**

**YOU NEED TO ASK THE "credential" folder ** that has to be added in src/main/resources. Without this folder, you CANNOT use the deployer. The email address to get the credentials are at the bottom of the following file. 


### USAGE ###

In order to use the deployer, you can use two different approaches:

  1. **Clone the git project and generate the executable:**
     - Convert the project into a Maven one if your IDE does not recognize it as a Maven Project;
     - run Maven install and Maven package;
     - look at the target folder of your project, you'll get a "lib" folder and the jar executable file.
     - in order to automatize the set up of the environmental variables and the execution of the broker, you can use the bash script “start_broker.sh” that is located under the “src/main/resources” path.
        
  2. **Download the zip folder containing the already generated files**
     - go [here]([Link Text](Link URL)https://bitbucket.org/cerict/specs-core-enforcement-broker/downloads) and download the zip file;
     - open your terminal and navigate to the downloaded folder;

    
Now you can run the deployer, through the two scripts you find inside the folder:


### SCRIPTS ###
## start_broker.sh ##
This script allow to acquire resources from a supported Cloud Service Provider. In particular it is able to handle the following inputs:

This script accepts 4 or 7 parameters:

  1. ** If only four parameters are provided:**
    - The first one has to be the provider endpoint;
    - The second one has to be the provider username;
    - The third one has to be the provider password;
    - The last one has to be the path to the implementation plan that has to be used.

  2. ** If seven inputs are provided:**
    - The first one has to be the FIRST provider endpoint;
    - The second one has to be the FIRST provider username;
    - The third one has to be the FIRST provider password;
    - The first one has to be the SECOND provider endpoint;
    - The second one has to be the SECOND provider username;
    - The third one has to be the SECOND  provider password;
    - they have to be passed with the following order [provider_endpoint, username, password];

Please consider that "FIRST" refers to the first provider that is defined in the implementation plan, and "SECOND" refers to the second provider that is defined in the implementation plan. 

NOTE: the script has to be placed in the same folder containing the broker jar file and the lib folder.

## deleteVMs .sh ##
This script allow to delete the VMs acquired, the security group associated and the subnetwork (if exsists) starting from the identifier used to acquire those resources; usually this identifier is the field \"sla_id\" defined inside the implementation_plan.

This script accepts till 4 parameters:

 1. **If no parameter is provided:**
    - an error is generated;
 2. **If four parameters are provided:**
    - the parameters have to be passed according to the following order [identifier, provider_endpoint, provider_username, provider_password];
    
### SUPPORT ###
If you need any support, or if you need the provider_endpoint and the credentials to use our cloud service provider, feel free to contact me via mail "[giancarlo.capone85@gmail.com](giancarlo.capone85@gmail.com)" or [maxrak@gmail.com](maxrak@gmail.com)